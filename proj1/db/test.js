const { Pool } = require('pg');
require('dotenv').config();

const connectionString = process.env.DB_URL//'postgres://proj1db_user:5dEFpNvPfN4DINHi3gpBM1R9tqlvFTlo@dpg-cdfcuj5a4992md4e4kh0-a.oregon-postgres.render.com/proj1db'//process.env.CONN_STRING
console.log(process.env.DB_URL)

const pool = new Pool({
  //user: 'proj1db_user',//process.env.DB_USER,
  //host: 'dpg-cdfcuj5a4992md4e4kh0-a',//process.env.DB_HOST,
  //database: 'proj1db',//process.env.DB_NAME,
  //password: '5dEFpNvPfN4DINHi3gpBM1R9tqlvFTlo',//String(process.env.DB_PASSWORD),
  //port: 5432,
  ssl : true,
  connectionString,
})

const query1 = {
  text: 'SELECT * FROM proj2table WHERE id = $1',
  values: ['1'],
  rowMode: 'array',
};

(async () => {
  try {
    console.log('TASK START')
    //console.log(q.values)
    //const res = await pool.query(q.text, q.values)
    const result = await pool.query(query1)
    console.log(result.rows)
    console.log('TASK DONE')
  } catch (err) {
    console.log(err.stack)
  }
})();

console.log(Number.isInteger(parseInt('1')))