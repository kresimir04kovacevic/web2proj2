import express from 'express';
import fs from 'fs';
import path from 'path'
import https from 'https';
import { auth, requiresAuth } from 'express-openid-connect'; 
import dotenv from 'dotenv'
import { Pool } from 'pg'
dotenv.config()

const app = express();
app.set("views", path.join(__dirname, "views"));
app.set('view engine', 'pug');

app.use(express.urlencoded())

const externalUrl = process.env.RENDER_EXTERNAL_URL;
const port = externalUrl && process.env.PORT ? parseInt(process.env.PORT) : 4080;

const connectionString = 'postgres://proj1db_user:5dEFpNvPfN4DINHi3gpBM1R9tqlvFTlo@dpg-cdfcuj5a4992md4e4kh0-a.oregon-postgres.render.com/proj1db'//process.env.DB_URL//'postgres://proj1db_user:5dEFpNvPfN4DINHi3gpBM1R9tqlvFTlo@dpg-cdfcuj5a4992md4e4kh0-a.oregon-postgres.render.com/proj1db'//process.env.CONN_STRING
//console.log(process.env.DB_URL)

const pool = new Pool({
  //user: 'proj1db_user',//process.env.DB_USER,
  //host: 'dpg-cdfcuj5a4992md4e4kh0-a',//process.env.DB_HOST,
  //database: 'proj1db',//process.env.DB_NAME,
  //password: '5dEFpNvPfN4DINHi3gpBM1R9tqlvFTlo',//String(process.env.DB_PASSWORD),
  //port: 5432,
  ssl : true,
  connectionString,
})

const config = { 
  authRequired : false,
  idpLogout : true, //login not only from the app, but also from identity provider
  secret: process.env.SECRET,
  baseURL: externalUrl || `https://localhost:${port}`,
  clientID: 'GdohVAAmkHY7Qfg9g0sH1SkTHWIh95tp',
  issuerBaseURL: 'https://dev-c7pibxseu5z2fg0n.us.auth0.com',
  clientSecret: 'lt0V2OzMEs-oQg5NUaCGtD7IM0KkjuXsbOk4kz5dXVnGBFDV_ipXZr6KuXybI1dA',
  authorizationParams: {
    response_type: 'code' ,
    //scope: "openid profile email"   
   },
};
// auth router attaches /login, /logout, and /callback routes to the baseURL
//app.use(auth(config));

app.get('/',  function (req, res) {
  let username : string | undefined;
  res.render('index', {username});
});

async function unsafeQuery(inputID:any) {
    try {
      console.log('unsafeQuery: '+inputID)
      const query1 = {
        text: "SELECT * FROM proj2table WHERE id = "+inputID+";",
        values: [],
        rowMode: 'array',
      };
      const result = await pool.query(query1)
      console.log(result.rows)
      return result.rows
    } catch (err) {
      console.log(err)
      return "No matches found!"
    }
}

async function safeQuery(inputID:any) {
  try {
    console.log('safeQuery: '+inputID)
    if(!/^\d+$/.test(inputID))return "No matches found!"
    const query1 = {
      text: "SELECT * FROM proj2table WHERE id = $1",
      values: [inputID],
      rowMode: 'array',
    };
    const result = await pool.query(query1)
    console.log(result.rows)
    return result.rows
  } catch (err) {
    console.log(err)
    return "No matches found!"
  }
}

app.get('/task1', function (req, res) {
  res.render('task1');
});

app.post('/task1', function (req, res) {
  console.log(req.body)
  var inputID = req.body.inputID
  var secure = req.body.secure
  if(inputID == ''){
    let msg = "Please input the ID!";
    res.render('task1', {msg});
  }else{
    if(secure != 'on'){
      let promise = unsafeQuery(inputID);
      promise.then((msg) => {console.log(msg); res.render('task1', {msg})})
    }else{
      let promise = safeQuery(inputID);
      promise.then((msg) => {console.log(msg); res.render('task1', {msg})})
    }
  }
});

if (externalUrl) {
  const hostname = '127.0.0.1';
  app.listen(port, hostname, () => {
  console.log(`Server locally running at http://${hostname}:${port}/ and from 
  outside on ${externalUrl}`);
  });
  }
  else {
  https.createServer({
  key: fs.readFileSync('server.key'),
  cert: fs.readFileSync('server.cert')
  }, app)
  .listen(port, function () {
  console.log(`Server running at https://localhost:${port}/`);
  });
  }
  