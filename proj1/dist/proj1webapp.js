"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var express_1 = __importDefault(require("express"));
var fs_1 = __importDefault(require("fs"));
var path_1 = __importDefault(require("path"));
var https_1 = __importDefault(require("https"));
var dotenv_1 = __importDefault(require("dotenv"));
var pg_1 = require("pg");
dotenv_1["default"].config();
var app = (0, express_1["default"])();
app.set("views", path_1["default"].join(__dirname, "views"));
app.set('view engine', 'pug');
app.use(express_1["default"].urlencoded());
var port = 4080;
var connectionString = process.env.DB_URL; //'postgres://proj1db_user:5dEFpNvPfN4DINHi3gpBM1R9tqlvFTlo@dpg-cdfcuj5a4992md4e4kh0-a.oregon-postgres.render.com/proj1db'//process.env.CONN_STRING
//console.log(process.env.DB_URL)
var pool = new pg_1.Pool({
    //user: 'proj1db_user',//process.env.DB_USER,
    //host: 'dpg-cdfcuj5a4992md4e4kh0-a',//process.env.DB_HOST,
    //database: 'proj1db',//process.env.DB_NAME,
    //password: '5dEFpNvPfN4DINHi3gpBM1R9tqlvFTlo',//String(process.env.DB_PASSWORD),
    //port: 5432,
    ssl: true,
    connectionString: connectionString
});
var config = {
    authRequired: false,
    idpLogout: true,
    secret: process.env.SECRET,
    baseURL: "https://localhost:".concat(port),
    clientID: 'GdohVAAmkHY7Qfg9g0sH1SkTHWIh95tp',
    issuerBaseURL: 'https://dev-c7pibxseu5z2fg0n.us.auth0.com',
    clientSecret: 'lt0V2OzMEs-oQg5NUaCGtD7IM0KkjuXsbOk4kz5dXVnGBFDV_ipXZr6KuXybI1dA',
    authorizationParams: {
        response_type: 'code'
    }
};
// auth router attaches /login, /logout, and /callback routes to the baseURL
//app.use(auth(config));
app.get('/', function (req, res) {
    var username;
    res.render('index', { username: username });
});
function unsafeQuery(inputID) {
    return __awaiter(this, void 0, void 0, function () {
        var query1, result, err_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    console.log('unsafeQuery: ' + inputID);
                    query1 = {
                        text: "SELECT * FROM proj2table WHERE id = " + inputID + ";",
                        values: [],
                        rowMode: 'array'
                    };
                    return [4 /*yield*/, pool.query(query1)];
                case 1:
                    result = _a.sent();
                    console.log(result.rows);
                    return [2 /*return*/, result.rows];
                case 2:
                    err_1 = _a.sent();
                    console.log(err_1);
                    return [2 /*return*/, "No matches found!"];
                case 3: return [2 /*return*/];
            }
        });
    });
}
function safeQuery(inputID) {
    return __awaiter(this, void 0, void 0, function () {
        var query1, result, err_2;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    console.log('safeQuery: ' + inputID);
                    if (!/^\d+$/.test(inputID))
                        return [2 /*return*/, "No matches found!"];
                    query1 = {
                        text: "SELECT * FROM proj2table WHERE id = $1",
                        values: [inputID],
                        rowMode: 'array'
                    };
                    return [4 /*yield*/, pool.query(query1)];
                case 1:
                    result = _a.sent();
                    console.log(result.rows);
                    return [2 /*return*/, result.rows];
                case 2:
                    err_2 = _a.sent();
                    console.log(err_2);
                    return [2 /*return*/, "No matches found!"];
                case 3: return [2 /*return*/];
            }
        });
    });
}
app.get('/task1', function (req, res) {
    res.render('task1');
});
app.post('/task1', function (req, res) {
    console.log(req.body);
    var inputID = req.body.inputID;
    var secure = req.body.secure;
    if (inputID == '') {
        var msg = "Please input the ID!";
        res.render('task1', { msg: msg });
    }
    else {
        if (secure != 'on') {
            var promise = unsafeQuery(inputID);
            promise.then(function (msg) { console.log(msg); res.render('task1', { msg: msg }); });
        }
        else {
            var promise = safeQuery(inputID);
            promise.then(function (msg) { console.log(msg); res.render('task1', { msg: msg }); });
        }
    }
});
https_1["default"].createServer({
    key: fs_1["default"].readFileSync('server.key'),
    cert: fs_1["default"].readFileSync('server.cert')
}, app)
    .listen(port, function () {
    console.log("Server running at https://localhost:".concat(port, "/"));
});
